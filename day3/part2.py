import re
from test import test_input
from utils import _indices_to_check

total_length = len(test_input)
line_length = 140
points = list()

gears = dict()
for match in re.finditer(r"\*", test_input):
    gears[match.start()] = {"numbers": 0, "ratio": 1}

for match in re.finditer("[0-9]+", test_input):
    start = match.start()
    end = match.end() - 1
    indices = _indices_to_check(start, end, total_length, line_length)

    for index in indices:
        if index in gears:
            gears[index]["numbers"] += 1
            gears[index]["ratio"] *= int(match.group())

for index, gear in gears.items():
    if gear["numbers"] == 2:
        points.append(gear["ratio"])

print(sum(points))

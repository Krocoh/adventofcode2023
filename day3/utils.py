def _indices_to_check(start, end, total_length, line_length):
    indices = list()

    # Not first character of line -> diagonals
    if start % line_length != 0:
        indices.append(start - 1)
        if start > (line_length - 1):
            indices.append(start - line_length - 1)
        if start < (total_length - line_length):
            indices.append(start + line_length - 1)

    # Not last character of line -> diagonals
    if (end + 1) % line_length != 0:
        indices.append(end + 1)
        if end > (line_length - 1):
            indices.append(end - line_length + 1)
        if end < (total_length - line_length):
            indices.append(end + line_length + 1)

    # Above and below each digit
    if start > (line_length - 1):
        for digit in range(start, end + 1):
            indices.append(digit - line_length)
    if start < (total_length - line_length):
        for digit in range(start, end + 1):
            indices.append(digit + line_length)

    return indices

import re
from test import test_input
from utils import _indices_to_check

total_length = len(test_input)
line_length = 140
points = list()

symbol_indices = set()
for match in re.finditer(r"[^0-9\.]", test_input):
    symbol_indices.add(match.start())

for match in re.finditer("[0-9]+", test_input):
    start = match.start()
    end = match.end() - 1
    indices = _indices_to_check(start, end, total_length, line_length)

    for index in indices:
        if index in symbol_indices:
            value = match.group()
            points.append(int(value))

print(sum(points))

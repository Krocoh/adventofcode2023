from test import test_input

test_input = test_input.splitlines()

points = list()

for line in test_input:
    split_line = line.split("|")
    winning_nums = set(split_line[0].split()[2:])
    my_nums = split_line[1].split()

    matching_nums = 0
    for num in my_nums:
        if num in winning_nums:
            matching_nums += 1

    if matching_nums > 0:
        points.append(pow(2, matching_nums - 1))

print(sum(points))

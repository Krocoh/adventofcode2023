from test import test_input

test_input = test_input.splitlines()

cards = {num: 1 for num in range(len(test_input))}

for index, line in enumerate(test_input):
    card_iters = cards[index]

    split_line = line.split("|")
    winning_nums = set(split_line[0].split()[2:])
    my_nums = split_line[1].split()

    matching_nums = 0
    for num in my_nums:
        if num in winning_nums:
            matching_nums += 1

    if matching_nums:
        for card_index in range(index + 1, index + matching_nums + 1):
            cards[card_index] += card_iters

print(sum(cards.values()))

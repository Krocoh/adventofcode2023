from test import test_input
from collections import Counter
from itertools import combinations_with_replacement

char_equivalents = {str(i): i for i in range(2, 10)}
char_equivalents.update({
    "T": 10,
    "J": 11,
    "Q": 12,
    "K": 13,
    "A": 14
})


def _compare_hands(hand1, hand2):
    values1 = sorted(Counter(hand1).values(), reverse=True)
    values2 = sorted(Counter(hand2).values(), reverse=True)

    if values1 == values2:
        for char1, char2 in zip(hand1, hand2):
            eq1 = char_equivalents[char1]
            eq2 = char_equivalents[char2]

            if eq1 > eq2:
                return hand1
            elif eq1 < eq2:
                return hand2

    if values1 > values2:
        return hand1
    return hand2


test_input = test_input.splitlines()
winning_values = list()

for line1, line2 in combinations_with_replacement(test_input, 2):
    hand1, bid1 = line1.split()
    hand2, bid2 = line2.split()

    if _compare_hands(hand1, hand2) == hand1:
        winning_values.append(int(bid1))
    else:
        winning_values.append(int(bid2))

print(sum(winning_values))

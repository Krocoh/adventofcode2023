from test import test_input
from collections import Counter
from itertools import combinations_with_replacement

char_equivalents = {str(i): i for i in range(2, 10)}
char_equivalents.update({
    "J": 0,
    "T": 10,
    "Q": 11,
    "K": 12,
    "A": 13
})


def _transfo_joker(values, jokers):
    if len(values) == 1:
        return values
    if len(values) == 5:
        return [2, 1, 1, 1]
    if len(values) == 4:
        return [3, 1, 1]
    if len(values) == 2:
        return [5]
    if values == [2, 2, 1] and jokers == 1:
        return [3, 2]
    return [4, 1]


def _compare_hands(hand1, hand2):
    jokers1 = hand1.count("J")
    values1 = sorted(Counter(hand1).values(), reverse=True)
    if jokers1:
        values1 = _transfo_joker(values1, jokers1)

    jokers2 = hand2.count("J")
    values2 = sorted(Counter(hand2).values(), reverse=True)
    if jokers2:
        values2 = _transfo_joker(values2, jokers2)

    if values1 == values2:
        for char1, char2 in zip(hand1, hand2):
            eq1 = char_equivalents[char1]
            eq2 = char_equivalents[char2]

            if eq1 > eq2:
                return hand1
            elif eq1 < eq2:
                return hand2

    if values1 > values2:
        return hand1
    return hand2


test_input = test_input.splitlines()
winning_values = list()

for line1, line2 in combinations_with_replacement(test_input, 2):
    hand1, bid1 = line1.split()
    hand2, bid2 = line2.split()

    if _compare_hands(hand1, hand2) == hand1:
        winning_values.append(int(bid1))
    else:
        winning_values.append(int(bid2))

print(sum(winning_values))

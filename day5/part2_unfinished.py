from test import test_input

test_input = test_input.splitlines()
test_input.append("")


categories = [list() for i in range(0, 8)]
categories[0] = zip(test_input[0].split()[::2], test_input[0].split()[1::2])
category_count = 0
remain_intervals = set()

for line in test_input[3:]:
    line_split = line.split()

    if len(line_split) == 0:
        for interval in remain_intervals:
            categories[category_count + 1].append(interval)
        continue
    if len(line_split) == 2:
        category_count += 1
        remain_intervals = set()
        continue

    dest = int(line_split[0])
    src = int(line_split[1])
    rng = int(line_split[2])

    for interval in categories[category_count]:
        diff_min = interval[0] - src
        diff_max = interval[1] - src

        if diff_min > rng or diff_max < 0:
            remain_intervals.add(interval)
            continue
        if diff_min < 0:
            diff_min = 0
            remain_intervals.add((interval[0], src - 1))
        if diff_max > rng:
            diff_max = rng
            remain_intervals.add((src + rng + 1, interval[1]))

        categories[category_count + 1].append((
            dest + diff_min, dest + diff_max
        ))

        to_remove = set()
        for remain in remain_intervals:
            if remain[0] == interval[0] and src - 1 < remain[1]:
                to_remove.add(remain)
            if remain[1] == interval[1] and remain[0] < src + rng + 1:
                to_remove.add(remain)
        for elem in to_remove:
            remain_intervals.remove(elem)

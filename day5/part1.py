from test import test_input

test_input = test_input.splitlines()
test_input.append("")

categories = [dict() for i in range(0, 8)]
seeds = test_input[0].split()[1:]
categories[0] = {i: int(seeds[i]) for i in range(0, len(seeds))}
category_count = 0

for line in test_input[3:]:
    line_split = line.split()

    if len(line_split) == 0:
        for item_id, elem in categories[category_count].items():
            if item_id not in categories[category_count + 1]:
                categories[category_count + 1][item_id] = elem
        continue
    if len(line_split) == 2:
        category_count += 1
        continue

    dest = int(line_split[0])
    src = int(line_split[1])
    rng = int(line_split[2])

    for item_id, elem in categories[category_count].items():
        diff = elem - src
        if diff in range(0, rng + 1):
            categories[category_count + 1][item_id] = dest + diff

print(min(categories[-1].values()))

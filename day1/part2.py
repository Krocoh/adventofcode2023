import re
from test import test_input

test_input = test_input.splitlines()

numbers = {
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9",
}

points = list()

for line in test_input:
    first_regex = "(?:[1-9]|one|two|three|four|five|six|seven|eight|nine)"
    first_pattern = re.compile(first_regex).search(line).group()
    first_digit = numbers.get(first_pattern, first_pattern)

    second_regex = (
        "(?:[1-9]|one(?!ight)|two(?!ne)|three(?!ight)|four|"
        "five(?!ight)|six|seven(?!ine)|eight(?!(?:wo|hree))|nine(?!ight))"
    )
    second_pattern = re.compile(second_regex).findall(line)[-1]
    second_digit = numbers.get(second_pattern, second_pattern)

    points.append(int(f"{first_digit}{second_digit}"))

print(sum(points))

import re
from test import test_input

test_input = test_input.splitlines()

points = list()

for line in test_input:
    first_digit = re.compile("[0-9]").search(line).group()
    second_digit = re.compile("[0-9](?=[^0-9]*$)").search(line).group()

    points.append(int(f"{first_digit}{second_digit}"))

print(sum(points))

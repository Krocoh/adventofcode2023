from test import test_input

test_input = test_input.splitlines()
total_time = int("".join(test_input[0].split()[1:]))
record = int("".join(test_input[1].split()[1:]))

time = total_time
speed = 0
while time * speed <= record:
    time -= 1
    speed += 1

print(2 * (int(total_time/2) - speed) + 1 + (total_time % 2))

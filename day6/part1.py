from test import test_input
from math import prod

test_input = test_input.splitlines()
races = zip(
    map(int, test_input[0].split()[1:]), map(int, test_input[1].split()[1:])
)
total_wins = list()

for total_time, record in races:
    time = total_time
    speed = 0
    while time * speed <= record:
        time -= 1
        speed += 1

    total_wins.append(2 * (int(total_time/2) - speed) + 1 + (total_time % 2))

print(prod(total_wins))

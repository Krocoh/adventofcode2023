import re
from test import test_input

test_input = test_input.splitlines()

max_balls = {
    "red": 12,
    "green": 13,
    "blue": 14
}

points = list()

for line in test_input:
    ball_regex = "[1-9]+ (?:red|green|blue)"
    draws = re.compile(ball_regex).findall(line)

    is_valid = True
    for draw in draws:
        split_draw = draw.split()
        if int(split_draw[0]) > max_balls[split_draw[1]]:
            is_valid = False
            break

    if is_valid:
        game_id = re.compile("[0-9]+").search(line).group()
        points.append(int(game_id))

print(sum(points))

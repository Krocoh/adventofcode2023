import re
from test import test_input

test_input = test_input.splitlines()

points = list()

for line in test_input:
    ball_regex = "[0-9]+ (?:red|green|blue)"
    draws = re.compile(ball_regex).findall(line)

    min_balls = {
        "red": 1,
        "green": 1,
        "blue": 1
    }
    for draw in draws:
        split_draw = draw.split()
        number = int(split_draw[0])
        color = split_draw[1]

        if number > min_balls[color]:
            min_balls[color] = number

    power = min_balls["red"] * min_balls["green"] * min_balls["blue"]
    points.append(power)

print(sum(points))
